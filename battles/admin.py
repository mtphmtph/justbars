from django.contrib import admin
from .models import Artist, Battle, Lyric

admin.site.register(Artist)
admin.site.register(Lyric)
admin.site.register(Battle)