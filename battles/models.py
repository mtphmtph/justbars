from django.db import models

class TimeStampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True

class Artist(TimeStampModel):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name

class Lyric(TimeStampModel):
    text = models.TextField()
    artist = models.ForeignKey(
        Artist, 
        related_name='artist', 
        on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.artist, self.pk)

    class Meta:
        ordering = ('artist', )

class Battle(TimeStampModel):  
    name = models.CharField(max_length=120)
    league = models.CharField(max_length=120)
    lyrics = models.ManyToManyField(Lyric)

    def __str__(self):
        return self.name
    