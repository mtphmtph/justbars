from django.shortcuts import render
from battles.models import Artist, Battle
from django.core import serializers
from django.http import JsonResponse
from django.template.loader import render_to_string

def home(request):
    context = {
        'artists': ['Aczino', 'Skone', 'Danger']
    }
    return render(request, 'home.html', context)

def battle_list(request):
    battles = Battle.objects.all().values()
    #data = serializers.serialize('json', battles)
    data = list(battles)
    # data = {
    #     'battles': battles
    # }
    return JsonResponse(data, safe=False)

def test(request):
    return render(request, "test.html", {})
