// Gulp configuration
// --------------------------

'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglyfly = require('gulp-uglyfly');

var paths = {
  sass: './assets/scss',
  js: './assets/js'
};

var sassOpts = {
  outputStyle: 'compressed',
  precison: 3,
  errLogToConsole: true
}

// Precompila SASS a CSS
gulp.task('sass', function () {
  return gulp.src(paths.sass + '/**/**/*.scss')
    .pipe(sass(sassOpts).on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch(paths.sass + '/**/**/*.scss', ['sass']);
});

gulp.task('js', function () {
    return gulp.src([
      paths.js + '/*.js',
      '!' + paths.js + '/app.min.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(uglyfly())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.js))
});

gulp.task('js:watch', function () {
  gulp.watch(paths.js + '/*.js', ['js']);
});

gulp.task('bundle', ['sass', 'js']);

gulp.task('bundle:watch', ['sass:watch', 'js:watch']);